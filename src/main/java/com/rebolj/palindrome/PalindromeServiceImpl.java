package com.rebolj.palindrome;

import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Math.min;

public class PalindromeServiceImpl implements PalindromeService {

    @Override
    @Nonnull
    public List<Palindrome> getPalindromes(@Nonnull String string, int n) {
        Preconditions.checkArgument(string != null);
        Preconditions.checkArgument(n > 0);
        if (string.isEmpty()) return Collections.emptyList();
        final ExpandedString expandedString = evaluateString(string);
        return toPalindromes(expandedString, n);
    }

    /**
     * Evaluate the source string and produce a list of palindrome lengths around each
     * central character, requiring O(n) runtime
     */
    private ExpandedString evaluateString(String string) {
        final ExpandedString expandedString = new ExpandedString(string);
        final int length = expandedString.getLength();

        int center = 0;
        int right = 0;

        for (int i = 0; i < length; i++) {
            final int mirror = 2 * center - i;

            if (right > i) {
                expandedString.setPalindromeLength(i, min(right - i, expandedString.getPalindromeLength(mirror)));
            }

            while (i + 1 + expandedString.getPalindromeLength(i) < expandedString.getLength() && i - 1 - expandedString.getPalindromeLength(i) >= 0 &&
                    expandedString.getCharacter(i + 1 + expandedString.getPalindromeLength(i)) == expandedString.getCharacter(i - 1 - expandedString.getPalindromeLength(i))) {
                expandedString.incrementPalindromeLength(i);
            }

            if (i + expandedString.getPalindromeLength(i)  > right) {
                center = i;
                right = i + expandedString.getPalindromeLength(i) ;
            }
        }

        return expandedString;
    }

    /**
     * Take an evaluated string, return n unique palindromes with greatest length,
     * requiring O(n*log(n)) runtime
     */
    List<Palindrome> toPalindromes(ExpandedString delimitedString, int n) {
        final List<Palindrome> palindromes = new LinkedList<>();

        for (int i = 0; i < delimitedString.getLength(); i++){
            if (delimitedString.getPalindromeLength(i) > 0) {
                final int length = delimitedString.getPalindromeLength(i);
                final char[] text = extractText(delimitedString, i, length);
                palindromes.add(new Palindrome(i/2 - length/2, length, new String(text)));
            }
        }

        palindromes.sort((o1, o2) -> o2.getLength() - o1.getLength());
        return palindromes.stream().distinct().limit(n).collect(Collectors.toList());
    }

    char[] extractText(ExpandedString delimitedString, int i, int length) {
        final char[] text = new char[length];
        int p = 0;
        for (int j = i - length; j < i + length; j++) {
            if (!delimitedString.isDelimiter(j)) {
                text[p++] = delimitedString.getCharacter(j);
            }
        }
        return text;
    }
}
