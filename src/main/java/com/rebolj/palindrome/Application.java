package com.rebolj.palindrome;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.util.List;

public class Application {
    @Option(name = "-t", aliases = { "--text" }, required = true, usage = "string to process for palindromes")
    private String text;

    @Option(name = "-n", aliases = { "--maxPalindromes" }, usage = "maximum number of unique palindromes to return")
    private int n = 3;

    private Application(String[] args) throws CmdLineException {
        CmdLineParser parser = new CmdLineParser(this);
        parser.parseArgument(args);

        final List<Palindrome> palindromes = new PalindromeServiceImpl().getPalindromes(text, n);
        palindromes.forEach(palindrome -> System.out.println(palindrome.toString()));
    }

    public static void main(String[] args)  {
        try {
            new Application(args);
        } catch (CmdLineException e) {
            System.out.println(e.getMessage());
        }
    }
}
