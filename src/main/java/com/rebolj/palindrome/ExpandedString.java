package com.rebolj.palindrome;

import java.util.Arrays;

class ExpandedString {
    private final StringCharacter[] stringCharacters;

    ExpandedString(String string) {
        this.stringCharacters = new StringCharacter[string.length() * 2 + 1];

        int i = 0;
        stringCharacters[i++] = new StringCharacter();

        while (i < stringCharacters.length - 1) {
            stringCharacters[i] = new StringCharacter(string.charAt((i++) / 2));
            stringCharacters[i++] = new StringCharacter();
        }
    }

    int getLength() {
        return stringCharacters.length;
    }

    void setPalindromeLength(int characterIndex, int length) {
        stringCharacters[characterIndex].setPalindromeLength(length);
    }

    int getPalindromeLength(int characterIndex) {
        return stringCharacters[characterIndex].getPalindromeLength();
    }

    void incrementPalindromeLength(int characterIndex) {
        stringCharacters[characterIndex].incrementPalindromeLength();
    }

    boolean isDelimiter(int characterIndex) {
        return stringCharacters[characterIndex].isDelimiter();
    }

    Character getCharacter(int characterIndex) {
        if (characterIndex < 0 || characterIndex > stringCharacters.length - 1) {
            return null;
        } else {
            return stringCharacters[characterIndex].getCharacter();
        }
    }

    @Override
    public String toString() {
        return Arrays.toString(stringCharacters);
    }

    private class StringCharacter {
        private final Character character;
        private int palindromeLength;

        StringCharacter(char character) {
            this.character = character;
            this.palindromeLength = 0;
        }

        StringCharacter() {
            this.character = null;
            this.palindromeLength = 0;
        }

        Character getCharacter() {
            return character;
        }

        int getPalindromeLength() {
            return palindromeLength;
        }

        void setPalindromeLength(int palindromeLength) {
            this.palindromeLength = palindromeLength;
        }

        void incrementPalindromeLength() {
            this.palindromeLength++;
        }

        boolean isDelimiter() {
            return this.character == null;
        }

        @Override
        public String toString() {
            return this.character + " - " + this.getPalindromeLength();
        }

    }
}
