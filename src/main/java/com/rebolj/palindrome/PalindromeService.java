package com.rebolj.palindrome;

import javax.annotation.Nonnull;
import java.util.List;

public interface PalindromeService {
    @Nonnull
    List<Palindrome> getPalindromes(@Nonnull String string, int n);
}
