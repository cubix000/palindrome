package com.rebolj.palindrome;

import com.google.common.base.Objects;

public class Palindrome {
    private int index;
    private int length;
    private final String text;

    Palindrome(int index, int length, String text) {
        this.index = index;
        this.length = length;
        this.text = text;
    }

    public int getIndex() {
        return index;
    }

    public int getLength() {
        return length;
    }

    public String getText() {
        return text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Palindrome)) return false;
        Palindrome that = (Palindrome) o;
        return length == that.length &&
                Objects.equal(text, that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(length, text);
    }

    @Override
    public String toString() {
        return "Text: " + text + ", " +
                "Index: " + index + ", " +
                "Length: " + length;
    }
}
