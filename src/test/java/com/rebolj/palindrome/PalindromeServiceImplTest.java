package com.rebolj.palindrome;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.*;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class PalindromeServiceImplTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private PalindromeService palindromeService;

    @Before
    public void setUp() throws Exception {
        palindromeService = new PalindromeServiceImpl();
    }

    @Test
    public void init() {
        palindromeService.getPalindromes("test", 1);
    }

    @Test
    public void nullInput() {
        thrown.expect(IllegalArgumentException.class);
        palindromeService.getPalindromes(null, 1);
    }

    @Test
    public void zeroSize() {
        thrown.expect(IllegalArgumentException.class);
        palindromeService.getPalindromes("test", 0);
    }

    @Test
    public void emptyInput() {
        assertTrue(palindromeService.getPalindromes("", 1).isEmpty());
    }

    @Test
    public void onlyUniqueResults() {
        assertThat(palindromeService.getPalindromes("aaa", 10),
                Matchers.contains(
                        new Palindrome(1, 3, "aaa"),
                        new Palindrome(1, 2, "aa"),
                        new Palindrome(1, 1, "a")
                ));
    }

    @Test
    public void singleCharacter() {
        assertThat(palindromeService.getPalindromes("a", 10),
                Matchers.contains(
                        new Palindrome(1, 1, "a")
                ));
    }

    @Test
    public void samplePalindrome() {
        assertThat(palindromeService.getPalindromes("sqrrqabccbatudefggfedvwhijkllkjihxymnnmzpop", 3),
                Matchers.contains(
                        new Palindrome(23, 10, "hijkllkjih"),
                        new Palindrome(13, 8, "defggfed"),
                        new Palindrome(5, 6, "abccba")
                ));
    }

    @Test
    public void macbethPalindrome() throws FileNotFoundException {
        palindromeService.getPalindromes(new Scanner(this.getClass().getClassLoader().getResourceAsStream("macbeth.ply")).useDelimiter("\\Z").next(), 100000);
    }

}
