# Palindrome Service

Test and build using Gradle:
~~~~
./gradlew test installDist
~~~~

Run the generated executable with the -t parameter for text (and optional -n for number or palindromes):
~~~~
./build/install/palindrome/bin/palindrome -t sqrrqabccbatudefggfedvwhijkllkjihxymnnmzpop
~~~~